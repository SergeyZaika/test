FROM ubuntu:18.04
RUN apt-get update && apt-get install wget apt-utils mariadb-server mariadb-client -y
RUN wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+bionic_all.deb && \
    dpkg -i zabbix-release_5.0-1+bionic_all.deb && apt-get update && \
    DEBIAN_FRONTEND='noninteractive' apt-get install zabbix-server-mysql zabbix-frontend-php zabbix-nginx-conf -y
#ADD create.sql.gz /usr/share/doc/zabbix-server-mysql/create.sql
COPY create.sql.gz /usr/share/doc/zabbix-server-mysql/
COPY nginx.conf /etc/zabbix/
COPY php-fpm.conf /etc/zabbix/
COPY script.sh /etc/init.d/
RUN chmod 777 /etc/init.d/script.sh && chmod 777 /etc/zabbix/nginx.conf && chmod 777 /etc/zabbix/php-fpm.conf
ENTRYPOINT service mysql start && /etc/init.d/script.sh start && /bin/bash
EXPOSE 80 
#EXPOSE 80 10050 10051
