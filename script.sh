#!/bin/bash
mysqladmin -u root password passwordforroot
mysql --user=root --password=passwordforroot -Bse "create database zabbix character set utf8 collate utf8_bin;create user zabbix@localhost identified by 'password';grant all privileges on zabbix.* to zabbix@localhost;"
zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -ppassword zabbix
echo 'DBPassword=password' >> /etc/zabbix/zabbix_server.conf
ln -s /etc/zabbix/nginx.conf /etc/nginx/sites-enabled/
rm -r /etc/nginx/sites-enabled/default
service zabbix-server start && service php7.2-fpm start && service nginx start && /bin/bash
